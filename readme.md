# Express Hello World
A project to try out implementing bitbucket pipelines and aws EC2 deployments.

## Install
`npm install`

## Scripts
`npm clean` Cleans dist dir
`npm build:babel` Transpiles JavaScript to Node v5 in dist dir.
`npm build:archive` Archives dist directory to express-hello-world.zip
`npm build` Runs clean, then all build scripts.

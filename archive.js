require('dotenv').config()
var fs = require('fs');
var archiver = require('archiver');
 
if (!process.env.ARCHIVE_FILENAME) {
  throw Error('Creating archive file failed, ARCHIVE_FILENAME environment variable must be set.')
}

// create a file to stream archive data to.
var output = fs.createWriteStream(__dirname + `/${process.env.ARCHIVE_FILENAME}`);
var archive = archiver('zip', {
  zlib: { level: 9 } // Sets the compression level.
});
 
// listen for all archive data to be written
// 'close' event is fired only when a file descriptor is involved
output.on('close', function() {
  console.log(archive.pointer() + ' total bytes');
  console.log('archiver has been finalized and the output file descriptor has closed.');
});
 
// This event is fired when the data source is drained no matter what was the data source.
// It is not part of this library but rather from the NodeJS Stream API.
// @see: https://nodejs.org/api/stream.html#stream_event_end
output.on('end', function() {
  console.log('Data has been drained');
});
 
// good practice to catch warnings (ie stat failures and other non-blocking errors)
archive.on('warning', function(err) {
  if (err.code === 'ENOENT') {
    console.warn(err.message)
  } else {
    // throw error
    throw err;
  }
});
 
// good practice to catch this error explicitly
archive.on('error', function(err) {
  throw err;
});
 
archive.directory('dist/', 'dist/')
archive.directory('install-scripts/', 'install-scripts/')
archive.file('readme.md')
archive.file('package.json')
archive.file('package-lock.json')
archive.file('appspec.yml')

// pipe archive data to the file
archive.pipe(output);

// finalize the archive (ie we are done appending files but streams have to finish yet)
// 'close', 'end' or 'finish' may be fired right after calling this method so register to them beforehand
archive.finalize();

const getGreeting = (name = 'human') => {
  return `Hello, ${name}.`
}

module.exports.getGreeting = getGreeting

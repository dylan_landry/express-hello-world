const { getGreeting } = require('../greet')
require('chai').should()

describe('greet', () => {

  describe('getGreeting()', () => {

    it('returns greeting with passed name.', () => {
      const expected = 'Dylan'
      const actual = getGreeting('Dylan')
      actual.should.include(expected)
    })

    it('defaults to greeting "human" when passed nothing', () => {
      const expected = 'human'
      const actual = getGreeting()
      actual.should.include(expected)
    })

  })
  
})

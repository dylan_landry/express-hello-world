const express = require('express')
const { getGreeting } = require('./greet')
const app = express()
const port = 3000

app.get('/', (req, res) => res.send(getGreeting()))

app.listen(port, () => console.log(`Example app listening on port ${port}!`))
